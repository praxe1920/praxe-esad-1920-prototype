<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>PraxESAD</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
      <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="page_content_menu">
            <nav class="main-nav">
              <ul>
                <li><a href="about.php"><img src="images/icons/white/home.png"

                      alt="" title=""><span>A Praxe</span></a></li>
                
                <li><a href="musicas.php"><img src="images/icons/white/video.png"

                      alt="" title=""><span>Músicas</span></a></li>
                <li><a href="saudacoes.php"><img src="images/icons/white/blog.png"

                      alt="" title=""><span>Saudações</span></a></li>
                <li><a href="perguntas.php"><img src="images/icons/white/tables.png"

                      alt="" title=""><span>Perguntas e Respostas</span></a></li>
                <li><a href="hierarquia.php"><img src="images/icons/white/form.png"

                      alt="" title=""><span>Hierarquia da Praxe</span></a></li>
                <li><a href="insignias.php"><img src="images/icons/white/slider.png"

                      alt="" title=""><span>Insígnias</span></a></li>
                <li><a href="apadrinhar.php"><img src="images/icons/white/toogle.png"

                      alt="" title=""><span>Apadrinhar</span></a></li>
                <li><a href="traje.php"><img src="images/icons/white/docs.png"

                      alt="" title=""><span>Traje Académico</span></a></li>

                  <li><a href="avisos.php"><img src="images/icons/white/aviso.png"

                      alt="" title=""><span>Avisos</span></a></li>      
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
