<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>Músicas</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"
                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"
                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <h2 class="page_title">Músicas</h2>
            <div class="page_content"> <br>
              <br>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  <span>HINO DA ESAD</span> </div>
                <div class="accordion-item-content">
                  <p></p>
                  ESAD, ESAD<br>
                  Meu rico ESAD<br>
                  A nota que tu nos levas<br>
                  Dava p’ra ter um carro de bois<br>
                  E continuar com estas merdas<br>
                  <p></p>
                  Ai, ai, ai, ai<br>
                  O lápis está-se acabar<br>
                  E estes filhos da puta,<br>
                  meu bem<br>
                  Não param de nos chular<br>
                  <p></p>
                  Ai, ai, ai, ai<br>
                  Temos que trabalhar<br>
                  Beber e beber<br>
                  É o que resta meu bem<br>
                  E é o que vem a calhar <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  <span>PRIMEIRO HINO DA ESAD (1991)</span> </div>
                <div class="accordion-item-content">
                  <p></p>
                  Todos os dias<br>
                  Quando acordo de manhã<br>
                  Que sofrimento<br>
                  Que sofrimento<br>
                  <p></p>
                  Não há quem acuda<br>
                  Quem me tira desta vida<br>
                  É um tormento<br>
                  É um tormento<br>
                  <p></p>
                  Os meus papás<br>
                  Dizem-me que isto é pró futuro<br>
                  Não vejo nada<br>
                  Não vejo nada<br>
                  <p></p>
                  É nestas alturas<br>
                  Que o impulso pró novo dia<br>
                  Chega....<br>
                  Pra me despertar....<br>
                  <p></p>
                  Eu ando na ESAD...<br>
                  Eu ando na ESAD...<br>
                  Eu ando na ESAD...<br>
                  Eu ando na ESAD...<br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  <span>A BESTA</span> </div>
                <div class="accordion-item-content">
                  <p></p>
                  Há quanto tempo estou<br>
                  p’ra ser praxado<br>
                  Ficar de 4 com um<br>
                  doutor em mim montado<br>
                  <p></p>
                  Chegar a casa todo o<br>
                  sujo e mal tratado<br>
                  Coitado de mim<br>
                  <p></p>
                  (refrão)<br>
                  Sou uma besta,<br>
                  Sou um besta,<br>
                  Vão-me torturar<br>
                  Ser praxado eu vou!<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  PATINHOS </div>
                <div class="accordion-item-content">
                  <p></p>
                  Todos os patinhos,<br>
                  Vão-se depenar, vão-se degolar,<br>
                  Os kits vamos vestir e de 4 vamos ficar,<br>
                  Os kits vamos sujar e com ovos vamos<br>
                  levar.<br>
                  <p></p>
                  É que a esta hora,<br>
                  É hora de ir correr, é hora de ir sofrer,<br>
                  Mas ainda é tempo p’ra todos conhecer,<br>
                  Mas ainda é tempo para conviver.<br>
                  <p></p>
                  Dux, Veteranos e Doutores,<br>
                  A Praxe nos vão dar, a Praxe nos vão dar,<br>
                  Com espírito e alegria p’ra Caloiro me tornar,<br>
                  Com sexo e amizade vamos acabar!<br>
                  <p></p>
                  (Grito da ESAD)<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  BRAÇOS NO AR </div>
                <div class="accordion-item-content">
                  <p></p>
                  BRAÇOS NO AR!<br>
                  <p></p>
                  (tum, tum, tum - palmas)<br>
                  <p></p>
                  TODOS DE PÉ!<br>
                  (tum, tum, tum - palmas)<br>
                  <p></p>
                  VAMOS CANTAR!<br>
                  (tum, tum, tum - palmas)<br>
                  <p></p>
                  ESAD ALLEZ!<br>
                  (...)<br>
                  ESAD ALLEZ!<br>
                  ESAD ALLEZ!<br>
                  ESAD ALLEZ!<br>
                  ESAD ALLEZ!<br>
                  ...<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  PÕE-TE DE QUATRO OH CALOIRO (Hotline bling) </div>
                <div class="accordion-item-content">
                  <p></p>
                  (Todos)<br>
                  Estamos aqui na ESAD<br>
                  A estourar mensalidade <br>
                  Viemos para estudar<br>
                  E à praxe fomos parar<br>
                  (Rapazes)<br>
                  Põe-te de quatro oh caloiro<br>
                  (Raparigas)<br>
                  Não não não poooonhoo não<br>
                  (Todos)<br>
                  Olha masé para o chão<br>
                  E aprende a lição<br>
                  Tudo vais ter d'aprender<br>
                  No julgamento dizer<br>
                  Põe-te de quatro oh caloiro.<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
ESAD
                  BALL </div>
                <div class="accordion-item-content">
                  <p></p>
                  ESAD serás sempre a primeira,<br>
                  Perante esta academia inteira. <br>
                  Verde e cor de rosa,<br>
                  sempre a representar!<br>
                  Força ESAD!<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  SETE ESAD'S </div>
                <div class="accordion-item-content">
                  <p></p>
                  Nós somos da ESAD<br>
                  Verde e cor de rosa<br>
                  Para a eternidade! <br>
                  Olé olé olé<br>
                  Nós somos da ESAD<br>
                  Artes e design<br>
                  Para a eternidade! <br>
                  Olé olé olé<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  SOMOS ESAD </div>
                <div class="accordion-item-content">
                  <p></p>
                  Somos ESAD (bater palmas) <br>
                  A faculdade (bater palmas)<br>
                  A melhor da academia!<br>
                  Para um dia ser doutor <br>
                  Eu grito ESAD com amor!<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  SUPER ESAD </div>
                <div class="accordion-item-content">
                  <p></p>
                  Nós somos caloiros e vamos gritar<br>
                  Artes e design não vamos parar <br>
                  Seja onde for não nos podem calar <br>
                  Por ti grande ESAD nós vamos cantaaaar<br>
                  Seja no sul ou no norte<br>
                  Somos a casa mais forte! <br>
                  Lalalalalalala <br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  QUEEN </div>
                <div class="accordion-item-content">
                  <p></p>
                  Estamos aqui para vos mostrar <br>
                  Que a praxe da ESAD é a melhor que há <br>
                  Verde e cor de rosa nós vamos gritar<br>
                  As outras casas vamos abafar<br>
                  Força força ESAD ESAD! (X2)<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  VAQUINHA </div>
                <div class="accordion-item-content">
                  <p></p>
                  Eu tenho uma vaca leiteira<br>
                  Não é uma vaca qualquer<br>
                  Dá-me leite e manteiguinha<br>
                  Mas que vaca tão fofinha<br>
                  Dlim dlom, dlim dlom<br>
                  <p></p>
                  Um chocalho se comprou<br>
                  E vaquinha dele gostou<br>
                  Dá passeios pelo prado<br>
                  Mata moscas com o rabo<br>
                  Dlim dlom, dlim dlom.<br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  TAXI </div>
                <div class="accordion-item-content">
                  <p></p>
                  Eu te conheci na ESAD<br>
                  A caminho da praxe (x2)<br>
                  E os doutores da praxe<br>
                  E os veteranos da praxe (x2)<br>
                  Me mandaram por de quatro<br>
                  E eu recusei<br>
                  <p></p>
                  Eu, eu, eu não vou abaixo (x2)<br>
                  Mas depois chegou o Dux<br>
                  E ai teve de ser<br>
                  Eu, eu, eu já vou abaixo! (x2)<br>
                  <p></p>
                  Não toca no preto caloiro (x3)<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  VACA LOUCA </div>
                <div class="accordion-item-content">
                  <p></p>
                  Tenho uma vaca qu’é louca<br>
                  Não é uma vaca qualquer<br>
                  Tem um parafuso a menos<br>
                  Parecido com o que nós temos<br>
                  Dlim dlom, dlim dlom<br>
                  <p></p>
                  Uma ração se comprou<br>
                  E a vaquinha dela gostou<br>
                  Ficou toda baralhada<br>
                  Mas que grande trapalhada<br>
                  Dlim dlom, dlim dlom<br>
                  <p></p>
                  Mas um dia ela foi-se<br>
                  E fiz dela uns bifinhos<br>
                  Quando comia a mioleira<br>
                  Deu-me logo diarreia<br>
                  Dlim dlom, dlim dlom<br>
                  <p></p>
                  Só depois vi como estava<br>
                  Tolinha (ou Tolinho) não parava <br>
                  Tremia por todo o lado<br>
                  E espumava-me como um entornado<br>
                  Dlim dlom, dlim dlom<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  AKA ESAD </div>
                <div class="accordion-item-content">
                  <p></p>
                  ESAD (grito)<br>
                  Um grito de guerra nós vamos gritar<br>
                  Verde (rapazes) , cor de rosa (raparigas) (4x)<br>
                  <p></p>
                  Esta academia nós vamos reinar<br>
                  Verde (rapazes) , cor de rosa (raparigas) (4x) <br>
                  ESAD (grito) <br>
                  <p></p>
                  Melhor que a ESAD não há (3x)<br>
                  Não ha , não há, nao há....<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  DOÇURA DE DOUTORES </div>
                <div class="accordion-item-content">
                  <p></p>
                  Ponho o kit, Tiro o kit,<br>
                  A hora que eles querem.<br>
                  Que paciência pequenina,<br>
                  Que doçura que eles têm.<br>
                  <p></p>
                  Ponho cedo, tiro à noite<br>
                  E às vezes à tardinha,<br>
                  A camisola do caloiro<br>
                  Cheira mesmo a merdinha.<br>
                  <p></p>
                  (2x)<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  VACA LOUCA </div>
                <div class="accordion-item-content">
                  <p></p>
                  Tenho uma vaca qu’é louca<br>
                  Não é uma vaca qualquer<br>
                  Tem um parafuso a menos<br>
                  Parecido com o que nós temos<br>
                  Dlim dlom, dlim dlom<br>
                  <p></p>
                  Uma ração se comprou<br>
                  E a vaquinha dela gostou<br>
                  Ficou toda baralhada<br>
                  Mas que grande trapalhada<br>
                  Dlim dlom, dlim dlom<br>
                  <p></p>
                  Mas um dia ela foi-se<br>
                  E fiz dela uns bifinhos<br>
                  Quando comia a mioleira<br>
                  Deu-me logo diarreia<br>
                  Dlim dlom, dlim dlom<br>
                  <p></p>
                  Só depois vi como estava<br>
                  Tolinha (ou Tolinho) não parava <br>
                  Tremia por todo o lado<br>
                  E espumava-me como um entornado<br>
                  Dlim dlom, dlim dlom<br>
                  <br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  CHEGUEI </div>
                <div class="accordion-item-content">
                  <p></p>
                  Cheguei
                  Sou Caloiro<br>
                  Fui praxado a tarde toda<br>
                  E estou tramado<br>
                  Caso a granada exploda<br>
                  Pois vai ser isto o resto do dia<br>
                  Avisa lá, pode falar<br>
                  <p></p>
                  Que eu cheguei<br>
                  Sou caloiro<br>
                  Fui praxado a tarde toda<br>
                  E estou tramado<br>
                  Caso a granada exploda<br>
                  Pois vai ser isto o resto do dia <br>
                  Avisa lá, que eu vou ficar<br>
                  <p></p>
                  Que eu cheguei com tudo<br>
                  Cheguei a partir tudo<br>
                  Vão-me praxar por de 4 até chorar<br>
                  A Academia pira<br>
                  A ESAD delira<br>
                  Pra rebentar acabamos de entrar<br>
                  <p></p>
                  Quem não gosta, senta e chora<br>
                  Hoje a ESAD vai arrasar<br>
                  Quem não gosta, senta e chora<br>
                  Que a nossa casa vai brilhar<br>
                  <p></p>
                  Cheguei<br>
                  Sou caloiro<br>
                  Fui praxado a noite toda<br>
                  Estou preparado<br>
                  Para a segunda ronda<br>
                  Somos ESAD, verde e cor-de-rosa<br>
                  Avisa lá, pode falar<br>
                  <p></p>
                  Que eu cheguei<br>
                  Sou caloiro<br>
                  Fui praxado a noite toda<br>
                  Estou preparado<br>
                  Para a segunda ronda<br>
                  Somos ESAD, verde e cor-de-rosa<br>
                  Avisa lá, que eu vou gritar<br>
                  <p></p>
                  Que eu cheguei com tudo<br>
                  Cheguei a partir tudo<br>
                  Vão-me praxar por de 4 até chorar<br>
                  A Academia pira<br>
                  A ESAD delira<br>
                  Pra rebentar acabamos de entrar<br>
                  <p></p>
                  Quem não gosta, senta e chora<br>
                  Hoje a ESAD vai arrasar<br>
                  Quem não gosta, senta e chora<br>
                  Que a nossa casa vai brilhar<br>
                  <p></p>
                  Cheguei<br>
                  Sou caloiro<br>
                  Fui praxado até agora<br>
                  Estou a arrasar<br>
                  Hoje não vou embora<br>
                  Pois vai ser isto o resto do ano<br>
                  Avisa lá, pode falar<br>
                  <p></p>
                  Atuei<br>
                  Na noite negra<br>
                  A representar a ESAD<br>
                  Pois verde e rosa<br>
                  É a melhor faculdade<br>
                  E mais disto só para o ano<br>
                  Avisa lá, pode falar<br>
                  Que arrasei<br>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  KIM POSSIBLE </div>
                <div class="accordion-item-content">
                  <p></p>
                  Ohh yeaaahhh
                  <p></p>
                  Sou caloiro da esad<br>
                  E ando na faculdade<br>
                  Todos juntos somos<br>
                  CA-LOI-RA-DA<br>
                  <p></p>
                  Nem o dux é demais para mim<br>
                  Nem que ele arme um chinfrim<br>
                  Sabes que estou a brincaaaaaar (hand flip)<br>
                  <p></p>
                  Seja qual for a nossa asneira<br>
                  Sabes que podes gritar<br>
                  PARA NOS PRAXAR<br>
                  <p></p>
                  Telefona para nos praxares <br>
                  Com uma mensagem também dá<br>
                  A qualquer momento de 4 (wink)<br>
                  Telefona para nos praxares <br>
                  <p></p>
                  Telefona, para nos praxares<br>
                  Seja onde for em qualquer lugar<br>
                  Vou chamar o dux<br>
                  Para nos PRAXAR<br>
                  <p></p>
                  Praxe constante<br>
                  Estou sempre presente<br>
                  Sabes que podes gritar<br>
                  PARA NOS PRAXAR<br>
                  <p></p>
                  como é que é Dux?<br>
                  (Phone hangs)??<br>
                  <br>
                  Telefona para nos praxares!<br>
                  <p></p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  D´ZRT </div>
                <div class="accordion-item-content">
                  <p></p>
                  Sinceramente <br>
                  Nao sei como é que te sentes<br>
                  Mas tenho as pernas dormentes<br>
                  E estou quase<br>
                  <p></p>
                  Honestamente<br>
                  Estou a ficar doente<br>
                  Com o fio que apanho<br>
                  Desde que cheguei aqui<br>
                  <p></p>
                  Consequentemente<br>
                  Metem me de 4<br>
                  Mandam me calar<br>
                  A olhar para o chão<br>
                  <p></p>
                  Ai Senhores doutores<br>
                  Vocês fazem me passar horrores<br>
                  O meu corpo já está cheio de dores<br>
                  Mas eu sei eu que não vou desistir<br>
                  <p></p>
                  Eu vou ficar aqui (x4)
                  <p></p>
                  Ai Caloirada<br>
                  Todos sabem que esta malta é tramada<br>
                  Mas a gente nunca fica parado<br>
                  E agora manda o traje para aqui<br>
                  <p></p>
                  Para praxar por ai (x4)
                  <p></p>
                  Começou, vê se entendes<br>
                  Não vale a pena mesmo que tu tentes<br>
                  Não aguento mais <br>
                  Ficar de 4<br>
                  Até o Dr. mandar<br>
                  <p></p>
                  Começou, tens de atingir<br>
                  Somos um só<br>
                  Começa a reagir<br>
                  Eu já saltei<br>
                  Eu tou na praxe<br>
                  Não vale a pena chorar <br>
                  <p></p>
                  Tanto tempo investido em ti<br>
                  Para poder acabar doutor<br>
                  <p></p>
                  Para mim tanto me fez<br>
                  Que mandem estar de 4 ou estar de 3<br>
                  Ou mesmo que inventem<br>
                  Algo que eu nunca fiz<br>
                  <p></p>
                  É beeeeeem
                  <p></p>
                  Não então caralho foda-se
                  <p></p>
                  Vou sofrer mais, vou <br>
                  Não tenho tempo<br>
                  Gosto de ti<br>
                  Nunca foste um contratempo<br>
                  Eu sou praxado e vou praxar<br>
                  Quando esta merda acabaaar<br>
                  <p></p>
                  Tanto tempo investido em ti<br>
                  Para poder acabar doutor
                  <p></p>
                  Para mim tanto me faz<br>
                  Já não vou voltar atrás<br>
                  <p></p>
                  
                </div>
              </div>
              <i class="icon icon-plus"> </i></div>
            <i class="icon icon-plus"> </i></div>
          <i class="icon icon-plus"> </i></div>
        <i class="icon icon-plus"> </i></div>
      <i class="icon icon-plus"> </i></div>
    <i class="icon icon-plus"> </i> <i class="icon icon-plus"> </i> <i class="icon icon-plus">
    </i>
      <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
