<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=windows-1252" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>Traje</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"

                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"

                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <h2 class="page_title">Traje Académico</h2>
            <br>
            <br>
            <div class="page_content">
              <p>Todo o estudante da Escola Superior de Artes e Design, quando
                tem a capa sobre os ombros ou traçada é obrigado a colocá-la de
                forma a que qualquer tipo de insígnia não seja visível. As
                dobras efectuadas são pelo número de matrículas e uma dobra a
                mais com significado livre para o estudante.</p>
              <h4>Quando a capa estiver traçada, só se pode ver preto</h4>
              <br>
              <p><b>A vida académica é feita de momentos e recordações</b>
                podendo ser retratadas pelos emblemas e insígnias (os quais
                devem ser bordados na capa, do lado interior esquerdo da mesma,
                com linha <b>PRETA</b>, em ponto de cruz ou ponto invisível).
                Os emblemas não devem, no entanto, ser colocados sem possuírem
                um significado académico ou pessoal para o estudante, pois esse
                tipo de emblemas não passam de acessórios que vão contra aquilo
                que o Traje Académico representa (não queiram parecer uma árvore
                de Natal). É proibido o uso de metal na capa, e a sua lavagem,
                visto que a capa simboliza todo o percurso académico
                universitário.</p>
              <div class="page_content">
                <div class="buttons-row"> <a href="#tab1" class="tab-link active button">
                    TRAJE FEMININO</a> <a href="#tab2" class="tab-link button">TRAJE
                    MASCULINO</a> </div>
                <div class="tabs-animated-wrap">
                  <div class="tabs">
                    <div id="tab1" class="tab active">
                      <h4>Traje Feminino</h4>
                      <p>- Sapatos pretos e sem qualquer adorno, lisos e de
                        tacão, de preferência com 3cm, podendo variar entre os 2
                        e os 5cm; <br>
                        - Meias de vidro pretas e lisas; <br>
                        - Saia preta de modelo simples e abaixo do joelho; <br>
                        - Camisa branca e lisa, de colarinho de modelo comum sem
                        botões; <br>
                        - Gravata preta lisa; <br>
                        - Casaco preto, de modelo simples, não cintado, com um
                        ou três botões; <br>
                        - Capa preta, lisa e sem forro, devendo ser usada
                        dobrada sobre o ombro esquerdo ou sobre os ombros. <br>
                        - Na lapela somente se pode usar o pin da ESAD, e apenas
                        em número ímpar e do lado direito; <br>
                        - Devem usar sempre o cabelo solto, com a exceção de dentro de casa onde é permitido usar o cabelo apanhado com um tóto simples preto ou transparente; <br>
                        - É proibido o uso de qualquer tipo de adorno excepto
                        anéis de compromisso ou casamento; caso não possa ser
                        retirado deve ser tapado ou escondido; <br>
                        - Dentro de casa é permitido o uso de unhas adornadas desde que seja exclusivamente utilizado o padrão simples Baby Boomer;<br>
                        - Nenhuma peça de roupa deve possuir etiquetas. </p>
                    </div>
                    <div id="tab2" class="tab">
                      <h4>Traje Masculino</h4>
                      <p> - Sapatos pretos de atacadores e corte liso, sem
                        aplicações e com número ímpar de buracos em cada lado; <br>
                        - Meias clássicas pretas; <br>
                        - Calça preta; <br>
                        - Colete preto sem abas ou de cerimónia, com número
                        ímpar de botões, sendo o último botão desapertado em
                        sinal de respeito aos mais velhos; <br>
                        - Camisa branca e lisa, de colarinho; <br>
                        - Gravata preta lisa; <br>
                        - Batina preta de botões pretos; <br>
                        - Capa preta, lisa e sem forro, devendo ser usada
                        dobrada sobre o ombro esquerdo ou sobre os ombros, <br>
                        - Na lapela somente pode usar o pin da ESAD; <br>
                        - Todos os que possuam cabelo comprido devem usá-lo
                        amarrado; <br>
                        - É proibido o uso de qualquer tipo de adorno excepto
                        anéis de compromisso ou casamento; <br>
                        - Somente o relógio de bolso é permitido, no colete; <br>
                        - Nenhuma peça de roupa deve possuir etiquetas; <br>
                        - O uso do Gorro da Praxe é facultativo. </p>
                    </div>
                  </div>
                </div>
                <hr> <br>
                <h4>O USO DO TRAJE</h4>
                <br>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span> EM TERMOS NORMAIS</span> </div>
                  <div class="accordion-item-content">
                    <p>A Capa, quando usada aos ombros, deve ter o
                      correspondente número de dobras ao número de matrículas
                      mais uma pela escola. Quando dobrada, a Capa usa-se sobre
                      o ombro esquerdo. Após o pôr-do-Sol não pode ser vista
                      qualquer parte branca do Traje, devendo portanto usar-se a
                      Capa Traçada.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span>EM SERENATAS OU FADO</span> </div>
                  <div class="accordion-item-content">
                    <p>Quando se ouvem os primeiros sons da guitarra não deve
                      haver Capa alguma que não esteja traçada! Não pode ser
                      vista qualquer parte branca do Traje.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span> EM CERIMÓNIAS ESPECIAIS</span> </div>
                  <div class="accordion-item-content">
                    <p>Em certas ocasiões, como bailes, as abas da batina podem
                      ser apertadas atrás, de forma a assemelharem-se a uma
                      “casaca de grilo”.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span> DURANTE O LUTO</span> </div>
                  <div class="accordion-item-content">
                    <p>A batina deve ser abotoada. Quanto à Capa deve usar-se
                      estendida sobre os ombros, sem quaisquer dobras e deve
                      apertarse- lhe as golas de forma a tapar a camisa. Deve
                      recolher-se todas as Insígnias para dentro da Pasta.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span> NA MISSA</span> </div>
                  <div class="accordion-item-content">
                    <p>Nunca se traça a Capa nesta cerimónia religiosa. Deve
                      usar-se estendida sobre os ombros, sem quaisquer dobras.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span> OS RASGÕES NA CAPA</span> </div>
                  <div class="accordion-item-content">
                    <p>É um costume muito antigo, o de rasgar a Capa. Podem ter
                      vários significados e dividem-se em três grupos: -Amigos,
                      colegas, cadeiras que custem a fazer promessas....
                      -Familiares, parentes, pessoas íntimas ao estudante... -Ao
                      centro: namorado(a) e/ou marido(esposa)</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span>RASGANÇO DA BATINA</span> </div>
                  <div class="accordion-item-content">
                    <p>Esta cerimónia, ainda que muito
representativa, terá caído um
pouco em desuso.
Tem lugar aquando do
último exame.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span> EMBLEMAS NO INTERIOR DA CAPA</span> </div>
                  <div class="accordion-item-content">
                    <p>Em número ímpar, usam-se na parte interior esquerda,
                      podendo representar os mais variados temas, desde que
                      relacionados com a vida académica, sendo que os mais
                      importantes deverão ser cosidos mais perto do coração.
                      Nenhum destes emblemas pode ser visível quando traçada a
                      Capa. Têm ser de cosidos em linha preta, em ponto de cruz
                      ou invisível.</p>
                  </div>
                </div>
                <hr>
                <h2 class="page_subtitle"> A Pasta Do Traje </h2>
                <div class="buttons-row"> <a href="#tab3" class="tab-link active button">Caloiro
                    </a> <a href="#tab4" class="tab-link button">Semi-Puto</a>
                  <a href="#tab5" class="tab-link button"> Puto</a> </div>
                <div class="tabs-simple">
                  <div class="tabs">
                    <div id="tab3" class="tab active">
                      <h4>Caloiro</h4>
                      <p> É vedado o uso da pasta da praxe. Ressalva-se que a
                        Pasta de Praxe só pode ser usada pelo caloiro depois da
                        sua primeira Queima das Fitas, razão pela qual se passa
                        a denominar Pastrano. </p>
                    </div>
                    <div id="tab4" class="tab">
                      <h4>Semi-Puto</h4>
                      <p> É permitido o uso da pasta de praxe, mas só podem
                        usá-la na mão, tendo o braço completamente estendido. </p>
                    </div>
                    <div id="tab5" class="tab">
                      <h4>Puto</h4>
                      <p> É permitido o uso de monograma na pasta da praxe. Esta
                        mesma norma aplica-se a todos os de hierarquia superior
                        a Puto. </p>
                    </div>
                  </div>
                </div>
                <br>
                <h4>OBRIGATÓRIO</h4>
                <br>
                <p><u>Os que usam a pasta da praxe devem trazer dentro dela,
                    pelo menos, um livro de estudo, uma sebenta ou um caderno de
                    apontamentos ou, na falta destes, um papel com o mínimo de
                    cinco palavras escritas pelo seu portador e material
                    riscador.</u></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
