<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>Comissius Praxis</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"
                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"
                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <div class="page_title_photos">
              <h2>Comissius Praxis</h2>
            </div>
            <div class="photo-categories"> </div>
            <div class="tabs-animated-wrap photos_tabs">
              <div class="tabs">
                <div id="tab1p" class="tab active">
                  <ul id="photoslist" class="photo_gallery_13">
                    <li><a rel="gallery-3" href="images/photos/photo1.jpg" title="Photo title"
                        class="swipebox"><img src="images/photos/photo1.jpg" alt="image"></a></li>
                    <li><a rel="gallery-3" href="images/photos/photo2.jpg" title="Photo title"
                        class="swipebox"><img src="images/photos/photo2.jpg" alt="image"></a></li>
                    <li><a rel="gallery-3" href="images/photos/photo3.jpg" title="Photo title"
                        class="swipebox"><img src="images/photos/photo3.jpg" alt="image"></a></li>
                    <li><a rel="gallery-3" href="images/photos/photo4.jpg" title="Photo title"
                        class="swipebox"><img src="images/photos/photo4.jpg" alt="image"></a></li>
                    <div class="clearleft"></div>
                  </ul>
                </div>
              </div>
            </div>
            <div class="clearleft"></div>
          </div>
        </div>
      </div>
    </div>
      <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
