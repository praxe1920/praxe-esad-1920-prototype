<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>Hierarquia</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"

                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"

                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <h2 class="page_title">Hierarquia de Praxe</h2>
            <br>
            <div class="page_content">
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>DUX VETERANORUM </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertence a esta categoria o Veterano que tiver sido
                      eleito como tal pelo Magnum Concilium Veteranorum.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>MAGNUM CONCILIUM VETERANORUM </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria todos os Dux’s que fazem parte
                      da Academia do Porto.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>DUX ESCOLASTICUS, FACULTIS OU INSTITUTUS </b> </span>
                  </div>
                  <div class="accordion-item-content">
                    <p>Pertence a esta categoria o Veterano que tiver sido
                      eleito como tal pelo Conselho de Veteranos da respectiva
                      Escola, Faculdade ou Instituto.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>CONCILIUM VETERANORUM </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os Veteranos que frequentaram
                      ou frequentam a Casa e foram eleitos para serem
                      conselheiros do Exmo Dux.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>VETERANO </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos que tenham um número
                      de matrículas superior ao necessário para tirar o
                      respectivo curso, e que tenham usado a Insígnia
                      correspondente ao terceiro ano três dias, seguidos ou não,
                      ou que como tal tenham sido considerados pelo Conselho de
                      Veteranos, por mérito académico.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>SEXTANISTA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos dos cursos com 6
                      anos de duração, que tenham 6 matrículas, em
                      estabelecimento de ensino superior português ou
                      estrangeiro, das quais duas, pelo menos, na Universidade
                      do Porto.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>MERDA de DOUTOR </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos matriculados no
                      último ano de mestrado.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>QUINTANISTA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos que tenham cinco
                      matrículas, não estando inscritos no segundo ano de
                      mestrado.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>DOUTOR de MERDA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos matriculados no
                      primeiro ano de mestrado.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>QUARTANISTA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos que tenham quatro
                      matrículas, não estando inscritos no primeiro ano de
                      Mestrado.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>PUTO </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta condição os alunos que tenham três
                      matrículas e estejam inscritos no terceiro ano do curso.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>TERCEIRANISTA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta condição os alunos que tenham três
                      matrículas, não estando inscritos no terceiro ano dos
                      respectivos cursos.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>SEMI-PUTO </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos que tenham duas
                      matrículas e estejam inscritos no segundo ano do curso.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>SEGUNDANISTA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria todos os alunos do primeiro
                      ciclo de cursos de três anos, tenham duas matrículas, mas
                      apenas durante o primeiro período de praxe. Ou sendo
                      estudantes do 1º ciclo de cursos com quatro anos, tenham
                      duas matrículas. </p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>CÃO </b> </span> </div>
                  <div class="accordion-item-content"> </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>PASTRANO </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria os alunos que foram caloiros
                      durante o ano lectivo anterior, no espaço que medeia entre
                      o dia do cortejo da queima das fitas e três dias antes da
                      abertura oficial do ano lectivo seguinte.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>CALOIRO ESTRANGEIRO </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria todos os alunos que, embora já
                      tendo estado matriculados num estabelecimento de ensino
                      superior, português ou estrangeiro, estando todavia
                      matriculados na ESAD.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>CALOIRO </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria todos os alunos que pela
                      primeira vez estejam matriculados em qualquer Curso
                      Superior.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>PÁRA-QUEDISTA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta categoria todos os alunos matriculados
                      no ano zero, na respectiva Escola, Faculdade ou Instituto.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>BESTA </b> </span> </div>
                  <div class="accordion-item-content">
                    <p>Pertencem a esta condição todos os alunos que não estejam
                      ainda matriculados em qualquer Curso Superior.</p>
                  </div>
                </blockquote>
              </div>
              <div class="accordion-item">
                <blockquote>
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> <b>POLÍCIA </b> </span> </div>
                  <div class="accordion-item-content"> </div>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
       <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
