<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>A Praxe</title>

    <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"
                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"
                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <h2 class="page_title">A Praxe</h2>
            <div class="page_content">
              <blockquote> <b>PARA VÓS CALOIRADA</b>
                <p> </p>
                <p>Sejam bem-vindos a esta mui nobre e honrada casa. SOU
                  CALOIRO......dizem vocês com a euforia irracional que vos é
                  característica. Nas vossas escolas, até ao momento, vocês eram
                  os mais sábios desses locais de pseudoinstituição. Mas neste
                  meio académico são os únicos que estão por baixo. Acima
                  estamos todos nós. Se até aqui só tinham sido os professores a
                  introduzir-vos um pouco de sabedoria nessas caixas de areia,
                  agora terão os... ...MERITÍSSIMOS DOUTORÍSSIMOS DOUTORES DA
                  PRAXE.</p>
              </blockquote>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  <span>A PRAXE</span> </div>
                <div class="accordion-item-content">
                  <p></p>
                  <p> ...é feita para ajudar os caloiros a terem uma rápida
                    integração no meio académico, ensinando-lhes as mais
                    elementares regras de amizade,solidariedade e excitação. </p>
                  <p>A PRAXE não é um “mar de rosas”, é um “mar de espinhos”,
                    mas também tem algumas pétalas das quais vos salientamos, a
                    Queima da Fitas. Este é um evento académico onde se
                    desenvolvem atividades culturais e inesquecíveis, juntando a
                    Academia e os Estudantes. </p>
                  <p>Já vos falamos do respeito e obediência que deverão ter aos
                    DOUTORES DA PRAXE e para salvaguardar quaisquer tentativas
                    da vossa parte de quebrar esta regra básica, ficam avisados
                    que serão severamente punidos.</p>
                  <p>Queremos ainda, para terminar este texto, que será de
                    díficil leitura e compreensão para vocês, dar-vos um
                    conselho: </p>
                  <p>Vivam a PRAXE e absorvam tudo de bom que ela vos dá, pois
                    só assim poderão, mais tarde, viver o academismo
                    condignamente. Assegurando-vos ainda que estes serão os
                    momentos de que terão as melhores recordações.</p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                  <span>MAS AFINAL O QUE É A PRAXE ACADÉMICA?</span> </div>
                <div class="accordion-item-content">
                  <p></p>
                  <p>Lá estamos nós novamente em Setembro, com o início do ano
                    letivo nas Universidades e com ele o tempo de tradição: as
                    praxes académicas. Todos nós já ouvimos falar sobre o que
                    são as Praxes. Para uns a Praxe é algo de negativo enquanto
                    que para outros é bastante positivo.</p>
                  <p>A Praxe Académica é um conjunto de tradições geradas entre
                    estudantes universitários e que já há séculos vem a ser
                    transmitida de geração em geração. É um Modus Vivendi
                    característico dos estudantes e que enriquece a cultura
                    Lusitana com tradições criadas e desenvolvidas pelos que nos
                    antecedem no uso da Capa e Batina. <br>
                    Praxe Académica é cultura herdada que nos compete preservar
                    e transmitir às próximas gerações.</p>
                  <p>A palavra Praxe tem origem na palavra grega PRAXIS que
                    significa a prática de tradições, dos usos e costumes. A
                    Praxe está de tal modo inserida no nosso quotidiano que
                    quando alguém procede de certa forma só porque era esperado
                    que assim o fizesse, não é raro ouvir-se dizer: “Pois... já
                    é da Praxe...”.</p>
                  <p>A história da Praxe remonta ao século XIV, praticada na
                    altura pelos clérigos monásticos, mas o seu contexto mais
                    conhecido aparece no século XVI sob o nome de “Investidas”.
                    <br>
                    A Praxe, na época, era na realidade bastante dura para com
                    os caloiros, o que levou a ser considerada “selvagem” pela
                    opinião popular nos finais do século XIX. </p>
                  <p>A Praxe revestiu-se historicamente de diversas formas.
                    Sofreu inúmeras transformações e chegou mesmo a estar
                    proibida e suspensa. Após o 25 de Abril de 1974 a
                    Universidade deixou de ser vista como “local sagrado”,
                    destinado a muito poucos. Assim, com a democratização da
                    Universidade, voltou a implementar-se a grande tradição da
                    Praxe Académica.</p>
                  <p>A Praxe serve para ajudar o caloiro ou recém-chegado à
                    Universidade a integrar-se no meio universitário, a criar
                    amizades e a desenvolver laços de sólida camaradagem. É
                    através da Praxe que o estudante desenvolve um profundo amor
                    e orgulho pela instituição que frequente, a sua segunda
                    casa.</p>
                  <p>A Praxe Académica e o seu espírito não é apenas das festas
                    e dos copos, ela também ajuda o estudante a preparar-se para
                    uma futura vida profissional. Através das várias “missões
                    impossíveis” que o caloiro tem de desempenhar, este vai-se
                    tornando cada vez mais desinibido, habituando-se a
                    improvisar em situações para as quais não estava preparado.</p>
                  <p>Não se pode confundir a Praxe Académica com as
                    “pseudopraxe”, executadas apenas por indivíduos ignorantes
                    na matéria. A Praxe não pode nunca ser sinónimo de atos de
                    violência barata levados a cabo por uns quantos frustrados,
                    que não sabem o que são as tradições académicas e só usam o
                    traje para se pavonearem na esperança de serem notados..<br>
                    São estes os indivíduos responsáveis pelo atual estado
                    moribundo da verdadeira Praxe Académica que tem vindo a dar
                    lugar a exageros e absurdos, um pouco por todo o lado. Que
                    partem de ignorantes que desejam que a Praxe seja aquilo que
                    lhes apetecer, sem terem noção das regras básicas e nenhum
                    respeito por aquilo que é a tradição da grande Praxe
                    Académica.</p>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span>QUEIMA DAS FITAS</span> </div>
                  <div class="accordion-item-content">
                    <p></p>
                    <p><b> ...É um momento que se vive intensamente</b> </p>
                    <p><b>...A ACADEMIA SAI TODA À RUA</b></p>
                    <p>Não se pode falar de Academismo sem referir o que, para
                      muitos, é o expoente máximo da vida de um estudante. É um
                      momento em que se vive intensamente e com fervor as
                      atividades que decorrem ao longo da Semana da Queima.<br>
                      <b> A semana começa com a Monumental Serenata que marca o
                        início da Queima. Aqui não se bate palmas. Deve-se
                        evitar fazer ruído.</b><br>
                      Segue-se a Imposição de Insígnias e o Serrote, o qual
                      serve para os finalistas desancarem naqueles que durante
                      todo o curso lhes fizeram por vezes a vida difícil. <br>
                      Reserva-se por vezes, um dia de Beneficência para ajudar
                      os menos afortunados, pondo em prática a solidariedade dos
                      estudantes.</p>
                    <p>Depois vem o cortejo onde a Academia sai toda à rua, o
                      trânsito para e o centro do Porto e arredores pertencem
                      aos estudantes.<br>
                      No Cortejo os finalistas lideram o “pelotão” de Cartola e
                      Bengala caso haja, o Carro do Curso ou da Escola
                      seguidamente por ordem de hierarquia até chegar aos
                      últimos (os caloiros).</p>
                    <p>Vivam a <b>PRAXE</b> e absorvam tudo de bom que ela vos
                      dá, pois só assim poderão viver, mais tarde, o academismo
                      condignamente. Asseguramos ainda que estes serão os
                      momentos de que terão as melhores recordações.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span>MANDAMENTOS DA PRAXE</span> </div>
                  <div class="accordion-item-content">
                    <p></p>
                    <p> ART. 1º / O caloiro nunca olha diretamente para um
                      DOUTOR, deve sempre olhar para o chão.</p>
                    <p>ART. 2º / O caloiro deve sempre cumprimentar o DOUTOR
                      quando este chega. (o cumprimento encontra-se registado
                      nesta app)</p>
                    <p>ART. 3º / O caloiro não ri, nem mostra a imunda
                      cremalheira que, na sua espécie, substitui os dentes
                      humanos.</p>
                    <p>ART. 4º / O caloiro não tem sexo, é assexuado.</p>
                    <p>ART. 5º / O caloiro não fuma, não bebe, visto ser
                      impossível efetuar estas ações com os cascos que possui.</p>
                    <p>ART. 6º / Sempre que um DOUTOR da ESAD solicitar um
                      voluntário para a execução de uma qualquer tarefa (decerto
                      importantíssima e extremamente bem planeada), todos os
                      caloiros são obrigatoriamente voluntários e
                      apresentar-se-ão com alegria e apreço berrando: Pronto,
                      pronto. Não empurrem, não empurrem! </p>
                    <p>ART. 7º / Sempre que assim seja solicitado por um DOUTOR,
                      o caloiro deverá responder de forma afirmativa berrando: <b>É
                        bem!</b> ou no caso da resposta ser negativa, com um
                      audível: <b>Não, então, caralho, foda-se!</b> </p>
                    <p>ART. 8º / A posição natural do caloiro é com as patas
                      dianteiras e traseiras assentes no chão (posição de
                      quadrúpede, mais conhecida por: de 4) </p>
                    <p><b>O ÚNICO direito do caloiro é o de escolher o seu
                        Padrinho/Madrinha.</b></p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                    <span>REGRAS DA HIERARQUIA</span> </div>
                  <div class="accordion-item-content">
                    <p></p>
                    <p> DEVE RESPEITAR A HIERARQUIA: A TODOS OS DOUTORES E
                      VETERANOS DA PRAXE!</p>
                    <p>- Deve respeitar sempre a hierarquia e em caso de abuso
                      de “poder”, deve comunicá-lo a um membro da Comissão de
                      Praxe; <br>
                      - Não deve rir muito à frente do Caloiro, deve virar-se de
                      costas, rir baixinho; <br>
                      - Não pode fumar nem beber em frente ao Caloiro; <br>
                      - Nunca poderá fazer qualquer tipo de observação a um
                      Doutor à frente de um Caloiro;<br>
                      - Não pode obrigar o Caloiro a pagar o que quer que seja;
                      <br>
                      - Qualquer desrespeito por parte dum Caloiro deverá ser
                      comunicada a um membro da Comissão de Praxe.</p>
                    <p>COM DUAS MATRICULAS : NÃO PODE PRAXAR!</p>
                    <p>- Não pode praxar (somente mandar o Caloiro olhar para o
                      chão ou descruzar os cascos, ou ainda calar-se caso este
                      esteja a falar com um outro caloiro). Só poderá praxar se
                      para tal for solicitado por um Doutor com mais matrículas;
                      <br>
                      - Tem como principal função a de fiscalizar os caloiros; <br>
                      - Em caso de dúvida deve dirigir-se a um membro da
                      Comissão de Praxe;</p>
                    <p>MAIS DE DUAS MATRICULAS :</p>
                    <p>- Sempre que desejar praxar deverá dirigir-se a quem
                      esteja a comandar a Praxe naquele momento; <br>
                      - Sempre que chegar atrasado ao local da Praxe deverá
                      dirigir-se a um membro da Comissão de Praxe para se
                      inteirar da situação no momento.</p>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                      <span>OS “DUXES” DA ESAD</span> </div>
                    <div class="accordion-item-content">
                      <p></p>
                      <p> 2015 - CHAIMITIS</p>
                      <p>2013 / 2015 BEZERRUM</p>
                      <p>2009+1 / 2013 KIKUS</p>
                      <p>2009 / 2009+1 MANGUEIRUS ESCULTORI </p>
                      <p>2005+1 / 2009 BOLIS DI BILHARIS</p>
                      <p>2003 / 2005+1 COELHUS</p>
                      <p>1999 / 2003 CALEIRAS</p>
                      <p>1997+1 / 1999 SUPER BOCK</p>
                      <p>1995+1 / 1997+1 GUGU</p>
                      <p>1993 / 1995+1 NENÉ</p>
                      <p>1991 / PATRÍCIA<br>
                        <b>Baptizada pela Faculdade de Engenharia da
                          Universidade do Porto como primeira representante
                          académica da Escola Superior de Artes e Design.</b></p>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-item-toggle"> <i class="icon icon-plus">+</i>&nbsp;
                      <span>AS CORES DAS FACULDADES</span> </div>
                    <div class="accordion-item-content">
                      <p></p>
                      <p><b>Escola Superior De Artes e Design</b><br>
                        Verde Bandeira e Rosa Claro</p>
                      <p><b>Faculdade de Belas Artes</b><br>
                        Magenta</p>
                      <p><b>Faculdade de Arquitetura</b><br>
                        Branco</p>
                      <p><b>Faculdade de Medicina</b><br>
                        Amarelo</p>
                      <p><b>Faculdade de Engenharia</b><br>
                        Tijolo</p>
                      <p><b>Faculdade Economia </b><br>
                        Vermelho e Branco</p>
                      <p><b>Faculdade de Desporto</b><br>
                        Castanho</p>
                      <p><b>Faculdade de Medicina Dentária</b><br>
                        Amarelo e Amarelo torrado</p>
                      <p><b>Faculdade de Psicologia e Ciências de Educação</b><br>
                        Laranja</p>
                      <p><b>Faculdade de Ciências</b><br>
                        Azul Claro</p>
                      <p><b>Faculdade de Letras</b><br>
                        Azul Escuro</p>
                      <p><b>Faculdade de Farmácia</b><br>
                        Roxo</p>
                      <p><b>Faculdade de Direito</b><br>
                        Vermelho</p>
                      <p><b>Faculdade de Ciências da Nutrição e Alimentação</b><br>
                        Amarelo e Verde</p>
                      <p><b>Escola Superior de Enfermagem do Porto</b><br>
                        Amarelo e Branco</p>
                      <p><b>Instituto de Ciências Biomédicas Abel Salazar</b><br>
                        Amarelo e Azul claro</p>
                      <p><b>Instituto Superior de Engenharia do Porto</b><br>
                        Cinza e Tijolo</p>
                      <p><b>Instituto Superior de Contabilidade e Administração
                          do Porto</b><br>
                        Vermelho e Azul escuro</p>
                      <p><b>Instituto Superior de Serviço Social do Porto</b><br>
                        Verde Água</p>
                      <p><b>Instituto Politécnico de Administração e Marketing</b><br>
                        Laranja e Azul</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
