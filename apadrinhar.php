<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>Apadrinhar</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"
                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"
                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <h2 class="page_title">Apadrinhar</h2>
            <div class="page_content">
              <blockquote> <b>CONDIÇÃO DE PADRINHOS OU MADRINHAS</b>
                <p> </p>
                <p>O Padrinho poderá ser todo o Doutor ou Veterano que o caloiro
                  decida. Assim sendo o caloiro terá de efectuar um humilde
                  pedido. O caloiro tem o direito de escolher o padrinho/os nas seguintes condições:<br>
		- Pode ter <b>dois padrinhos</b> sendo que deverão ser de <b>sexos opostos</b>,<br>
		- No caso de apenas <b>um padrinho</b> este poderá ser do <b>mesmo sexo ou do sexo oposto</b>.<br>
			  </p>
                <p>Anteriormente, um(a) padrinho/ madrinha <u>NÃO PODIA TER</u>,
                  em cada ano, um <u>NÚMERO DE AFILHADOS(AS) SUPERIOR A TRÊS</u>.
                  <u>ACTUALMENTE</u>, não há limite de afilhados por Padrinho/
                  Madrinha. </p>
                <p><b>A função dos Padrinhos é a de orientar o Caloiro durante
                    toda a sua vida académica. Os Padrinhos deverão estar
                    presente no baptismo dos seus afilhados, sendo também
                    responsável pela comparência destes no Tribunal da Praxe
                    caso algum dos seus afilhados receba ordem para tal
                    comparência. Os Padrinhos não deverão proteger o caloiro
                    durante a praxe, mas salvaguardá-lo de eventuais abusos ao
                    regulamento da mesma.</b></p>
                <p></p>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
      <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
