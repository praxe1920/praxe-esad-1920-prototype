<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>Insígnias</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"

                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"

                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <h2 class="page_title">Insígnias</h2>
            <div class="page_content">
              <blockquote> <b>INSÍGNIAS PESSOAIS</b>
                <p> </p>
                <p>As Insígnias Pessoais devem ser usadas de acordo com as cores
                  do curso.<br>
                  Na ESAD as cores atribuídas ao curso e escola são: ROSA CLARO:
                  Artes VERDE BANDEIRA: “Novas Tecnologias” que inclui o Design.</p>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span>+ SEMENTE</span> </div>
                  <div class="accordion-item-content">
                    <p>É a primeira Insígnia, agora meramente simbólica: são
                      fitas de algodão com as cores do curso, com um nó cerrado
                      ao centro do seu comprimento; pode ser cosida na Capa após
                      a 2ª matrícula, dado a 1ª Insígnia oficial ser a Nabiça.</p>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span>+ NABIÇA</span> </div>
                  <div class="accordion-item-content">
                    <p>É a segunda Insígnia, agora a 1ª oficial; são fitas
                      iguais à da semente, mas mais compridas e dispostas em
                      laço, com duas pontas; a quando da sua substituição deve
                      ser cosida à Capa.</p>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                      <span>+ GRELO</span> </div>
                    <div class="accordion-item-content">
                      <p>É a terceira Insígnia (segunda oficial) e a primeira
                        que se usa na Pasta de Praxe; deve ter aproximadamente
                        três metros e termina com três nós e cinco meios laços,
                        sendo que ao puxar pelas pontas não se poderão desfazer
                        os laços, sob pena de sanção de unhas.</p>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                      <span>+ FITAS</span> </div>
                    <div class="accordion-item-content">
                      <p>É a Insígnia correspondente ao actual 3ºano
                        (anteriormente no 1ºano de Mestrado no caso da ESAD) e
                        são oito fitas largas de seda, dispostas alternadamente
                        na Pasta de Praxe; aos estudantes finalistas, após a
                        Páscoa, são-lhe assinadas essas fitas.</p>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                      <span>+ CARTOLA E BENGALA</span> </div>
                    <div class="accordion-item-content">
                      <p>Estas Insígnias distinguem o finalistas de qualquer
                        curso; no caso dos rapazes junta-se um laço, e uma
                        roseta no caso das raparigas.</p>
                    </div>
                  </div>
                </div>
              </blockquote>
              <blockquote> <b>INSÍGNIAS DA PRAXE</b>
                <p> </p>
                <p></p>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> COLHER</span> </div>
                  <div class="accordion-item-content">
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                    <span> MOCA</span> </div>
                  <div class="accordion-item-content">
                   </div>
                  <div class="accordion-item">
                    <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                      <span> TESOURA</span> </div>
                    <div class="accordion-item-content">
                      </div>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-item-toggle"> <i class="icon icon-plus"></i>&nbsp;
                      <span> PENICO</span> </div>
                    <div class="accordion-item-content">
                      </div>
                  </div>
                </div>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
     <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
