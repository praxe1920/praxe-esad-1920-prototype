<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

//include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      href="apple-touch-startup-image-640x1096.png">
    <title>Saudações</title>
      <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors/turquoise.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css">
    <link type="text/css" rel="stylesheet" href="css/animations.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900"
      rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="pages">
      <div data-page="projects" class="page no-toolbar no-navbar">
        <div class="page-content">
          <div class="navbarpages">
            <div class="nav_left_logo"><a href="menu.php"><img src="images/logo.png"
                  alt="" title=""></a></div>
            <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png"
                  alt="" title=""></a></div>
          </div>
          <div id="pages_maincontent">
            <h2 class="page_title">Saudações</h2>
            <br>
            <br>
            <div class="page_content">
              <blockquote> <b>SAUDAÇÃO AO DUX VETERANORUM</b>
                <p> </p>
                <p>AVE DUX VETERANORUM PRAXITURE TE SALUTE !</p>
              </blockquote>
              <br>
              <blockquote> <b>SAUDAÇÃO AO MAGNUM CONCILIUM VETERANORUM</b>
                <p> </p>
                <p>AVE MAGNUM CONSILIUM VETERANORUM PRAXITURE TE SALUTE !</p>
              </blockquote>
              <br>
              <blockquote> <b>SAUDAÇÃO AO DUX</b>
                <p> </p>
                <p>AVE DUX ESCOLASTICUS PRAXITURE TE SALUTE !</p>
              </blockquote>
              <br>
              <blockquote> <b>SAUDAÇÃO CONCILIUM VETERANORUM</b>
                <p> </p>
                <p>AVE CONSILIUM VETERANORUM PRAXITURE TE SALUTE !</p>
              </blockquote>
              <br>
              <blockquote> <b>SAUDAÇÃO COMISSÃO DE PRAXE</b>
                <p> </p>
                <p>AVE COMISSIUS PRAXIS PRAXITURE TE SALUTE !</p>
              </blockquote>
              <br>
              <blockquote> <b>CUMPRIMENTO AOS VETERANOS DA PRAXE</b>
                <p> </p>
                <p>BOM DIA (BOA TARDE / BOA NOITE) EXCELENTÍSSIMOS DOUTORÍSSIMOS
                  VETERANOS DA PRAXE, OMNIPOTENTE, SALVÉ, HIP-HIP URRA, OIÉ,
                  FIXE, NICE, BUÉ !</p>
              </blockquote>
              <br>
              <blockquote> <b>CUMPRIMENTO AOS DOUTORES DA PRAXE</b>
                <p> </p>
                <p>BOM DIA (BOA TARDE / BOA NOITE) EXCELENTÍSSIMOS DOUTORÍSSIMOS
                  DOUTORES DA PRAXE, OMNIPOTENTE, SALVÉ, HIP-HIP URRA, OIÉ,
                  FIXE, NICE, BUÉ ! </p>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
  </body>
</html>
